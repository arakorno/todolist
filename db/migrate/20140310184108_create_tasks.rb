class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :title, :null => false
      t.boolean :isDone, :default => false
      t.datetime :deadline
      t.references :todolist

      t.timestamps
    end
    add_index :tasks, :todolist_id
  end
end
