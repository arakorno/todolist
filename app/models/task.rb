class Task < ActiveRecord::Base
  belongs_to :todolist
  attr_accessible :deadline, :isDone, :title
  validates :title, presence: true,
            length: { minimum: 1 }
end
