class TasksController < ApplicationController
  def create
    @todolist = Todolist.find(params[:todolist_id])
    @task = @todolist.tasks.create(task_params)
    respond_to do |format|
      format.html
      format.xml  { render :xml => @task}
      format.js
    end
  end

  def destroy
    @todolist = Todolist.find(params[:todolist_id])
    @task = @todolist.tasks.find(params[:id])
    @task_id = params[:id];
    @task.destroy
    respond_to do |format|
      if @task.save
          format.html {redirect_to(@task)}
          format.xml  { render :xml => @task }
          format.js
      else
          format.html {redirect_to(@task)}
          format.xml  { render :xml => task.errors, :status => :unprocessable_entity }
          format.js
      end
    end
  end

  def update_title
    @todolist = Todolist.find(params[:todolist_id])
    @task = @todolist.tasks.find(params[:id])
    @task.update_column('title', params[:title])
    render json: @task

  end

  def update_status
    @todolist = Todolist.find(params[:todolist_id])
    @task = @todolist.tasks.find(params[:id])
    @task.update_column('isDone', params[:status])
    render json: @task

  end

  private
  def task_params
    params.require(:task).permit(:title)
  end


end


