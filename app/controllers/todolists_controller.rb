class TodolistsController < ApplicationController
  def index
    @todolists = Todolist.all
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @todolists }
    end
  end

  def new
    @todolist = Todolist.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @todolist }
    end
  end

  def create
    @todolist = Todolist.new(params[:todolist])

    respond_to do |format|
      if @todolist.save
        format.html { redirect_to(@todolist, :notice => 'Todolist was successfully created.') }
        format.xml  { render :xml => @todolist, :status => :created, :location => @todolist }
        format.js
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @todolist.errors, :status => :unprocessable_entity }
        format.js
      end
    end
  end

  def destroy
    @todolist = Todolist.find(params[:id])
    @todolist.destroy
    respond_to do |format|
      format.html {redirect_to(@todolist)}
      format.xml  { render :xml => @todolist }
      format.js
    end
  end
end
