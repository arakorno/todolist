var taskId, todolistId, lastFocused;

$('a').live('click', function(){
    {
        var allData = $(this)[0];
        taskId = allData.getAttribute('data-task-id');
        todolistId = allData.getAttribute('data-todolist-id');
        $("#persent_task_title_"+taskId).hide();
        $("#new_task_title_"+taskId).show();
        $('#new_title_'+taskId).focus();
    }
});


$('input[type=text]').live('focusin', function(){
    lastFocused = $(this).attr('id');

});

$('input[type=text]').live("focusout",function(){
    var newTitle = $('#'+lastFocused).val();
    if(newTitle.length>0 && lastFocused.indexOf("new_title_")==0){
        if (confirm("Are you sure to change task' title?")) {
                updateTaskTitle(newTitle);
        }

    }

    $("#persent_task_title_"+taskId).show();
    $("#new_task_title_"+taskId).hide();


});

function updateTaskTitle(title){
    var url = "http://rails-task-manager.herokuapp.com/todolists/"+todolistId+"/tasks/"+taskId;
    $.ajax({
        url: url,
        dataType: "json",
        type: "PUT",
        contentType: "application/json",
        data: '{\"title\":\"' + title + '\",\"todolist_id\":\"' + todolistId + '\",\"id\":\"'+ taskId +'\"}',
        success: function(task) {
            $("#persent_task_title_"+task.id).html(task.title);
        }
    });
}
