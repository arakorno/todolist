var taskId, todolistId

$('input[type="checkbox"]').live('change', function () {
    var allData = $(this)[0];
    taskId = allData.getAttribute('data-task-id');
    todolistId = allData.getAttribute('data-todolist-id');
    var check = $(this).attr('checked');
    var status = check=="checked" ? 1 : 0;
    updateTaskStatus(status);
});


function updateTaskStatus(status){
    var url = "http://rails-task-manager.herokuapp.com/todolists/"+todolistId+"/tasks/"+taskId+'/status';
    $.ajax({
        url: url,
        dataType: "json",
        type: "PUT",
        contentType: "application/json",
        data: '{\"status\":\"' + status + '\",\"todolist_id\":\"' + todolistId + '\",\"id\":\"'+ taskId +'\"}',
        success: function(task) {
            var content = status == 1 ? '<strike>'+ task.title  +'</strike>' :  task.title;
            $("#persent_task_title_"+task.id).html(content);
        }
    });
}